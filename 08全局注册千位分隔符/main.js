import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

Vue.filter('division', function (data) {
  data = data + ''
  return data.replace(/(\d)(?=(\d{3})+$)/g, '$1,')
})
new Vue({
  render: h => h(App),
}).$mount('#app')
