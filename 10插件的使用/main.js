import Vue from "vue";
import App from "./App.vue";

import direvtive from '@/main/direvtive'
import filter from '@/main/directive'

Vue.config.productionTip = false;

Vue.use(direvtive)
Vue.use(filter)
// let res = 'laoshi'
// Vue.directive('power', {
//   inserted(data, el) {
//     console.log(data, el);
//     if (!el.value.includes(res)) {
//       data.remove()
//     }
//   }
// })
new Vue({
  render: (h) => h(App),
}).$mount("#app");
