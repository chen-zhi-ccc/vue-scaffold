import Vue from "vue";
import App from "./App.vue";

import directive from './main/directive'
import filter from './main/filter'
Vue.config.productionTip = false;
Vue.use(directive)
Vue.use(filter)
new Vue({
  render: (h) => h(App),
}).$mount("#app");
