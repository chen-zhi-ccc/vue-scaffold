export default function (Vue) {
    Vue.filter('division', function (data) {
        data = data + ''
        return data.replace(/(\d)(?=(\d{3})+$)/g, '$1,')
    })
}