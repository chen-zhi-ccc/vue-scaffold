import Vue from "vue";
import App from "./App.vue";

import disective from '@/main/disective'
import filter from '@/main/filter'
Vue.config.productionTip = false;
Vue.use(disective)
Vue.use(filter)
new Vue({
  render: (h) => h(App),
}).$mount("#app");
