import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        num: 0,
        count: 1
    }, mutations: {
        add(state) {
            state.num++
        },
        addn(state, val) {
            state.num += val.n
        }
    },
    actions() { },
    getters: {}
})