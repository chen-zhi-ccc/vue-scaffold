import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        num: 0,
        count: 1
    },
    mutations: {
        add(state) {
            state.num++
        },
        addn(state, val) {
            console.log(val);
            state.num += val.n
        }
    },
    actions: {
        waitAdd(store) {
            setTimeout(() => {
                store.commit('add')
            }, 1000)
        },
        waitAddn({ commit }, payload) {
            setTimeout(() => {
                commit('addn', payload)
            }, 1000)
        }
    },
    getters: {}
})