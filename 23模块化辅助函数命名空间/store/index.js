import Vue from 'vue'
import Vuex from 'vuex'

import num from './modules/num'
import movie from './modules/movie'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        num,
        movie
    }
})