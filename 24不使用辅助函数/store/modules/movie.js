let state = {}
let mutations = {}
let actions = {}
let getters = {}
export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}