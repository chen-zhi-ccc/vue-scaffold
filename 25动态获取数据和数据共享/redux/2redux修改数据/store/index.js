import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        count: 0
    },
    mutations: {
        add(state) {
            state.count++
        },
        addN(state, val) {
            state.count += val.n
        }
    },
    actions: {
        waitAdd({ commit }) {
            setTimeout(() => {
                commit(['add'])
            }, 1000)
        }
    },
    getters: {

    }
})