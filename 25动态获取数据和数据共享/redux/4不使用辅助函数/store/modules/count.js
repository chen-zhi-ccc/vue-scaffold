let state = {
    count: 0

}
let mutations = {
    add(state) {
        state.count++
    },
    addN(state, val) {
        state.count += val.n
    }
}
let actions = {
    waitAdd({ commit }) {
        setTimeout(() => {
            commit('add')
        }, 1000)
    },
    waitAddN({ commit }, n) {
        setTimeout(() => {
            commit('addN', n)
        }, 1000)
    }
}
let getters = {
    counts({ count }) {
        // console.log(count);
        return count *= count
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}