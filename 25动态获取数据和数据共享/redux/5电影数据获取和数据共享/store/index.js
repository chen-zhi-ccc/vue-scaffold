import Vue from 'vue'
import Vuex from 'vuex'

import count from './modules/count'
import movie from './modules/movie'
Vue.use(Vuex)
import persistedstate from 'vuex-persistedstate'
export default new Vuex.Store({
    modules: {
        count,
        movie
    },
    plugins: [persistedstate()]
})