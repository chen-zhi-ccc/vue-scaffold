let initstate = {
    count: 0
}
let state = JSON.parse(localStorage.getItem('count')) || initstate
let mutations = {
    add(state) {
        state.count++
        localStorage.setItem('count', JSON.stringify(state))
    },
    addN(state, val) {
        state.count += val.n
        localStorage.setItem('count', JSON.stringify(state))
    }
}
let actions = {
    waitAdd({ commit }) {
        setTimeout(() => {
            commit('add')
        }, 1000)
    },
    waitAddN({ commit }, n) {
        setTimeout(() => {
            commit('addN', n)
        }, 1000)
    }
}
let getters = {
    counts({ count }) {
        // console.log(count);
        return count *= count
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}