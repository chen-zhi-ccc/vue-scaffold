let state = {
    count: 1

}
let mutations = {
    add(state) {
        state.count++
    },
    addn(state, res) {
        state.count += res.n
    },
}
let actions = {
    waitadd({ commit }) {
        setTimeout(() => {
            commit('add')
        }, 1000)
    },
    waitaddn({ commit }, n) {
        setTimeout(() => {
            commit('addn', n)
        }, 1000)
    }
}
let getters = {
    cuns({ count }) {
        return count *= count
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}