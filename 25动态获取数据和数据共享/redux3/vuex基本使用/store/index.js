import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        count: 1
    },
    mutations: {
        add(state) {
            state.count++
        },
        addn(state, val) {
            state.count += val.n
        },

    },
    actions: {
        waitadd({ commit }) {
            setTimeout(() => {
                commit('add')
            }, 1000)
        },
        waitaddn({ commit }, n) {
            setTimeout(() => {
                commit('addn', n)
            }, 1000)
        }
    },
    getters: {
        twoCount({ count }) {
            // return Math.sqrt(state.count)
            return count *= count
        }
    }
})