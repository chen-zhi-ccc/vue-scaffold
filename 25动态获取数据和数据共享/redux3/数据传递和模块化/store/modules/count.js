let initstate = {
    count: 1
}
let state = JSON.parse(localStorage.getItem('count')) || initstate
let mutations = {
    add(state) {
        state.count++
        localStorage.setItem('count', JSON.stringify(state))
    },
    addn(state, val) {
        state.count += val.n
        localStorage.setItem('count', JSON.stringify(state))

    },
}
let actions = {
    waitadd({ commit }) {
        setTimeout(() => {
            commit('add')
        }, 1000)
    },
    waitaddn({ commit }, n) {
        setTimeout(() => {
            commit('addn', n)
        }, 1000)
    }
}
let getters = {
    twoCount({ count }) {
        // return Math.sqrt(state.count)
        return count *= count
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}