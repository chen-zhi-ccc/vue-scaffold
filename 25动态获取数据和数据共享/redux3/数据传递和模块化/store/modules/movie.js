import axios from "axios"

let state = {
    movieList: []
}
let mutations = {
    setMovie(state, val) {
        state.movieList = val.movieList
    }
}
let actions = {
    async getMovie({ commit }) {
        let res = await axios.get(`https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a`)
        // console.log(res);
        commit('setMovie', { movieList: res.data.data.list })
    }
}
let getters = {}
export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
}