import Vue from 'vue'
import Vuex from 'vuex'

import num from './modules/num'
import movie from './modules/movie'
Vue.use(Vuex)
// 插件自动数据持久化
import persistedstate from 'vuex-persistedstate'
export default new Vuex.Store({
    modules: {
        num,
        movie
    },
    plugins: [persistedstate()]
})