let state = {
    num: 0,
    count: 1
}
let mutations = {
    add(state) {
        state.num++
    },
    addn(state, val) {
        state.num += val.n
    }
}
let actions = {
    waitAdd(store) {
        setTimeout(() => {
            store.commit('add')
        }, 1000)
    },
    waitAddn({ commit }, payload) {
        setTimeout(() => {
            commit('addn', payload)
        }, 1000)
    }
}
let getters = {
    numsqrt(state) {
        return Math.sqrt(state.num)
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}