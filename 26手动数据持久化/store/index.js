import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import count from './modules/count'
import movie from './modules/movie'
export default new Vuex.Store({
    modules: {
        count,
        movie
    }
})