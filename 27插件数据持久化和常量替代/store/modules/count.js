// 插件数据持久化
import { ADD, ADDN } from '../mutations_types'
let state = {
    count: 1
}
let mutations = {
    [ADD](state) {
        state.count++
    },
    [ADDN](state, val) {
        state.count += val.n
    },
}
let actions = {
    waitadd({ commit }) {
        setTimeout(() => {
            commit('add')
        }, 1000)
    },
    waitaddn({ commit }, n) {
        setTimeout(() => {
            commit('addn', n)
        }, 1000)
    }
}
let getters = {
    twoCount({ count }) {
        // return Math.sqrt(state.count)
        return count *= count
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}