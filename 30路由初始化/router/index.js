import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Home from '../pages/Home'
import Login from '../pages/Login'

export default new VueRouter({
    routes: [
        { path: '/home', component: Home },
        { path: '/login', component: Login },
    ]
})