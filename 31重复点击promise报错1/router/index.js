import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)



import Home from '../pages/Home'
import Login from '../pages/Login'
import Music from '../pages/Home/Music'
import News from '../pages/Home/News'
import Play from '../pages/Home/Play'
export default new VueRouter({
    routes: [
        {
            path: '/home',
            component: Home,
            name: 'home',
            children: [
                {
                    path: 'news',
                    component: News,
                    name: 'news'
                },
                {
                    path: 'music',
                    component: Music,
                    name: 'music'
                },
                {
                    path: 'play',
                    component: Play,
                    name: 'play'
                },
            ]
        },
        {
            path: '/login',
            component: Login,
            name: 'login'
        },
    ]
})