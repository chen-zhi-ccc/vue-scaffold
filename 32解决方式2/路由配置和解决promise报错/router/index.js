import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
// push报错
let pushs = VueRouter.prototype.push
VueRouter.prototype.push = function (location, onComplete = () => { }, onAbort = () => { }) {
    return pushs.call(this, location, onComplete, onAbort)
}
// replace报错
let repla = VueRouter.prototype.replace
VueRouter.prototype.replace = function (location, onComplete = () => { }, onAbort = () => { }) {
    return repla.call(this, location, onComplete, onAbort)
}
import Home from '../pages/Home'
import Login from '../pages/Login'
import News from '../pages/Home/News'
import Play from '../pages/Home/Play'
import Music from '../pages/Home/Music'
export default new VueRouter({
    routes: [
        {
            path: '/home',
            component: Home,
            name: 'home',
            children: [
                {
                    path: 'news',
                    component: News,
                    name: 'news'
                },
                {
                    path: 'play',
                    component: Play,
                    name: 'play'
                },
                {
                    path: 'music',
                    component: Music,
                    name: 'music'
                },
            ]
        },
        {
            path: '/login',
            component: Login,
            name: 'login'
        },
    ]
})