import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
let pushs = VueRouter.prototype.push
VueRouter.prototype.push = function (local, arr1 = () => { }, arr2 = () => { }) {
    return pushs.call(this, local, arr1, arr2)
}
let replaces = VueRouter.prototype.replace
VueRouter.prototype.replace = function (local, arr1 = () => { }, arr2 = () => { }) {
    return replaces.call(this, local, arr1, arr2)
}
import Home from '../pages/Home'
import Login from '../pages/Login'
import News from '../pages/Home/News'
import Music from '../pages/Home/Music'
import Play from '../pages/Home/Play'
export default new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', redirect: '/home' },
        {
            path: '/home', component: Home, redirect: '/home/news', children: [
                // { path: '', redirect: 'news' },
                { path: 'news', component: News, name: 'news' },
                { path: 'play', component: Play, name: 'play' },
                { path: 'music', component: Music, name: 'music' },
            ]
        },
        { path: '/login', component: Login, name: 'login' }
    ]
})