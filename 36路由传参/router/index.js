import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
let pushs = VueRouter.prototype.push;
VueRouter.prototype.push = function (local, arr1 = () => {}, arr2 = () => {}) {
  return pushs.call(this, local, arr1, arr2);
};
let replaces = VueRouter.prototype.replace;
VueRouter.prototype.replace = function (
  local,
  arr1 = () => {},
  arr2 = () => {}
) {
  return replaces.call(this, local, arr1, arr2);
};
import Home from "../pages/Home";
import Login from "../pages/Login";
import News from "../pages/Home/News";
import Music from "../pages/Home/Music";
import Play from "../pages/Home/Play";
import MusicItem from "../pages/Home/Music/Item";
import NewsItem from "../pages/Home/News/Item";

export default new VueRouter({
  mode: "history",
  routes: [
    { path: "/", redirect: "/home" },
    {
      path: "/home",
      component: Home,
      redirect: "/home/news",
      children: [
        // { path: '', redirect: 'news' },
        {
          path: "news",
          component: News,
          name: "news",
          children: [
            { path: "sport/:name/:price?", component: NewsItem, name: "sport" },
            { path: "money/:name/:price?", component: NewsItem, name: "money" },
            {
              path: "military/:name/:price?",
              component: NewsItem,
              name: "military",
            },
          ],
        },
        { path: "play", component: Play, name: "play" },
        {
          path: "music",
          component: Music,
          name: "music",
          children: [
            { path: "jay", component: MusicItem, name: "jay" },
            { path: "jj", component: MusicItem, name: "jj" },
            { path: "eson", component: MusicItem, name: "eson" },
          ],
        },
      ],
    },
    { path: "/login", component: Login, name: "login" },
  ],
});
