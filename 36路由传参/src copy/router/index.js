import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
import Home from "../pages/Home";
import Login from "../pages/Login";

// 路由懒加载设置
let News = () => {
  return import("../pages/Home/News");
};
let Music = () => {
  return import("../pages/Home/Music");
};
let Play = () => {
  return import("../pages/Home/Play");
};
let MusicItem = () => {
  return import("../pages/Home/Music/Item");
};
let NewsItem = () => {
  return import("../pages/Home/News/Item");
};
// 重复点击promise报错
let pushs = VueRouter.prototype.push;
VueRouter.prototype.push = function (local, arr1 = () => {}, arr2 = () => {}) {
  return pushs.call(this, local, arr1, arr2);
};

let repla = VueRouter.prototype.replace;
VueRouter.prototype.replace = function (
  local,
  arr1 = () => {},
  arr2 = () => {}
) {
  return repla.call(this, local, arr1, arr2);
};

export default new VueRouter({
  mode: "history",
  routes: [
    { path: "/", component: Home },
    {
      path: "/home",
      component: Home,
      name: "home",
      props: {
        name: "张三",
      },
      children: [
        { path: "", component: News },
        {
          path: "news",
          component: News,
          name: "news",
          children: [
            { path: "newItem/:id/:age?", component: NewsItem, name: "newItem" },
          ],
        },
        {
          path: "music",
          component: Music,
          name: "music",
          //   配置query传参的路由
          children: [
            { path: "musicName", component: MusicItem, name: "musicName" },
          ],
        },
        { path: "play", component: Play, name: "play" },
      ],
    },
    { path: "/login", component: Login, name: "login" },
  ],
});
