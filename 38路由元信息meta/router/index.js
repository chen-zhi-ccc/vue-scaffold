import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
import Home from "../pages/Home";
import Login from "../pages/Login";

// 路由懒加载设置
let News = () => import("../pages/Home/News");
let Music = () => import("../pages/Home/Music");
let Play = () => import("../pages/Home/Play");
let MusicItem = () => import("../pages/Home/Music/Item");
let NewsItem = () => import("../pages/Home/News/Item");

// 重复点击promise报错
let pushs = VueRouter.prototype.push;
VueRouter.prototype.push = function (local, arr1 = () => {}, arr2 = () => {}) {
  return pushs.call(this, local, arr1, arr2);
};

let repla = VueRouter.prototype.replace;
VueRouter.prototype.replace = function (
  local,
  arr1 = () => {},
  arr2 = () => {}
) {
  return repla.call(this, local, arr1, arr2);
};

export default new VueRouter({
  mode: "history",
  routes: [
    { path: "/", redirect: "/home" },
    {
      path: "/home",
      component: Home,
      name: "home",
      meta: {
        title: "主页",
      },
      props: {
        nam: "ssss",
        sex: "男",
      },
      children: [
        { path: "", redirect: "news" },
        {
          path: "news",
          component: News,
          name: "news",
          props: true,
          meta: { title: "新闻" },
          // params传参
          children: [
            {
              path: "newItem/:id/:age?",
              component: NewsItem,
              name: "newItem",
              props: true,
            },
          ],
        },
        {
          path: "music",
          component: Music,
          name: "music",
          //   配置query传参的路由
          children: [
            {
              path: "musicName",
              component: MusicItem,
              name: "musicName",
              props: (route) => {
                return {
                  ...route.query,
                  a: 10,
                  b: 20,
                };
              },
            },
          ],
        },
        { path: "play", component: Play, name: "play" },
      ],
    },
    { path: "/login", component: Login, name: "login" },
  ],
});
