import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
import Home from "../pages/Home";
import Login from "../pages/Login";

// 路由懒加载设置
let News = () => {
  return import("../pages/Home/News");
};
let Music = () => {
  return import("../pages/Home/Music");
};
let Play = () => {
  return import("../pages/Home/Play");
};
let MusicItem = () => {
  return import("../pages/Home/Music/Item");
};
let NewsItem = () => {
  return import("../pages/Home/News/Item");
};
// 重复点击promise报错
let pushs = VueRouter.prototype.push;
VueRouter.prototype.push = function (local, arr1 = () => {}, arr2 = () => {}) {
  return pushs.call(this, local, arr1, arr2);
};

let repla = VueRouter.prototype.replace;
VueRouter.prototype.replace = function (
  local,
  arr1 = () => {},
  arr2 = () => {}
) {
  return repla.call(this, local, arr1, arr2);
};

let router = new VueRouter({
  mode: "history",
  routes: [
    { path: "/", component: Home },
    {
      path: "/home",
      component: Home,
      name: "home",

      // 路由props传参
      props: {
        name: "xxx",
        sex: "aa",
      },
      children: [
        { path: "", component: News },
        {
          path: "news",
          component: News,
          name: "news",
          beforeEnter: (to, from, next) => {
            console.log("路由独享守卫");
            next();
          },
          children: [
            {
              path: "newItem/:id/:age?",
              component: NewsItem,
              name: "newItem",
              // 布尔值可以直接params接收参数
              props: true,
            },
          ],
        },
        {
          path: "music",
          component: Music,
          name: "music",
          //   配置query传参的路由
          children: [
            {
              path: "musicName",
              component: MusicItem,
              name: "musicName",
              // 组件元信息meta传参
              meta: {
                title: "musicName",
              },
              // 传输传参默认带一个$route参数，结构就可以获取到值
              props: (route) => {
                return {
                  ...route.query,
                  xx: 111,
                };
              },
            },
          ],
        },
        { path: "play", component: Play, name: "play" },
      ],
    },
    { path: "/login", component: Login, name: "login" },
  ],
});

router.beforeEach((to, from, next) => {
  console.log("全局路由守卫");
  next();
});
router.beforeResolve((to, from, next) => {
  console.log("全局解析守卫");
  next();
});
router.afterEach(() => {
  console.log("全局后置钩子，只有两个参数to, from");
});
export default router;
