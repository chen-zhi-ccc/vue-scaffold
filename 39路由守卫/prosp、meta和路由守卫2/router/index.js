import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
import Home from "../pages/Home";
import Login from "../pages/Login";

// 路由懒加载设置
let News = () => {
  return import("../pages/Home/News");
};
let Music = () => {
  return import("../pages/Home/Music");
};
let Play = () => {
  return import("../pages/Home/Play");
};
let MusicItem = () => {
  return import("../pages/Home/Music/Item");
};
let NewsItem = () => {
  return import("../pages/Home/News/Item");
};
// 重复点击promise报错
let pushs = VueRouter.prototype.push;
VueRouter.prototype.push = function (local, arr1 = () => {}, arr2 = () => {}) {
  return pushs.call(this, local, arr1, arr2);
};

let repla = VueRouter.prototype.replace;
VueRouter.prototype.replace = function (
  local,
  arr1 = () => {},
  arr2 = () => {}
) {
  return repla.call(this, local, arr1, arr2);
};

let router = new VueRouter({
  mode: "history",
  routes: [
    { path: "/", component: Home },
    {
      path: "/home",
      component: Home,
      name: "home",
      beforeEnter: (to, from, next) => {
        console.log("路由独享守卫");
        next();
      },
      // props对象传参
      props: {
        name: "张三",
      },
      children: [
        { path: "", component: News },
        {
          path: "news",
          component: News,
          name: "news",
          children: [
            {
              path: "newItem/:id/:age?",
              component: NewsItem,
              name: "newItem",
              // prosp布尔值传参，只能传递params参数
              props: true,
            },
          ],
        },
        {
          path: "music",
          component: Music,
          name: "music",
          //   配置query传参的路由
          children: [
            {
              path: "musicName",
              // meta路由元信息传递参数
              meta: {
                title: "musicName",
              },
              component: MusicItem,
              name: "musicName",
              // props函数传参
              props: (route) => {
                return {
                  ...route.query,
                  sex: "男",
                };
              },
            },
          ],
        },
        { path: "play", component: Play, name: "play" },
      ],
    },
    { path: "/login", component: Login, name: "login" },
  ],
});
router.beforeEach((to, from, next) => {
  console.log(
    "全局路由前置守卫",
    to,
    "to是切换到的 路由,from是之前的路由",
    from
  );
  next();
});
router.beforeResolve((to, from, next) => {
  console.log("全局解析守卫", to, from);
  next();
});
router.afterEach((to, from) => {
  console.log("全局后置钩子,只有两个参数", to, from);
});
export default router;
