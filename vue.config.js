const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    open: true,
    port: "8000", //端口号配置
    host: "127.0.0.1",
  },
});
